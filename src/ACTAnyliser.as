package
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public final class ACTAnyliser
	{
		public function ACTAnyliser()
		{
		}

		public function initialize():void
		{
		}

		//Char 		1Byte
		//Short		2Byte
		//Int			4Byte
		public function startAnylise(_actFileByte:ByteArray):void
		{
			//@see http://help.adobe.com/zh_CN/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7d54.html
			//大头小头->数据流格式
			_actFileByte.endian=Endian.LITTLE_ENDIAN;
			var fileHeader:int=_actFileByte.readShort()
			var fileVersion:int=_actFileByte.readByte();
			readyUnknowByte(_actFileByte, 1);
			var animationNum:int=_actFileByte.readUnsignedShort();
			readyUnknowByte(_actFileByte, 10);
			for (var i:int=0; i < animationNum; i++)
			{
				var subChildNum:int=_actFileByte.readInt();
				readyUnknowByte(_actFileByte, 32);
			}
		}

		private function readyUnknowByte(_byte:ByteArray, _size:int):void
		{
			_byte.position+=_size;
		}


	}
}
