package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;

	public class RoACTAnylister extends Sprite
	{
		private var mTempFileLoader:URLLoader;

		public function RoACTAnylister()
		{
			mTempFileLoader=new URLLoader();
			mTempFileLoader.dataFormat=URLLoaderDataFormat.BINARY;
			mTempFileLoader.load(new URLRequest("asset/4_man_galtun.act"));
			mTempFileLoader.addEventListener(Event.COMPLETE, onLoadActFileComplate);
		}

		private function onLoadActFileComplate(e:Event):void
		{
			var anylser:ACTAnyliser=new ACTAnyliser();
			anylser.initialize();
			anylser.startAnylise(mTempFileLoader.data);
		}

	}
}
